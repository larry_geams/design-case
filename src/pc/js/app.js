import hack from './module/hack.js';
hack();

import smoothScroll from './module/smooth-scroll.js';
smoothScroll(); 

import stickyHeader from './module/sticky-header.js';
stickyHeader();

import scrollMagic from './module/scroll-magic.js';
scrollMagic(); 
 