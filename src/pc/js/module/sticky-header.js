export default function stickyHeader() {

    'use strict';

    let $body = $('body');
    let $header = $('.l-header');

    const $document = $(document);
    const $headerOffset = $header.offset().top;
    const $headerHeight = $header.height();

    $document.on('scroll', function() {

        let $self = $(this);
        const $scrollPosition = $self.scrollTop(); 

        if ( $headerOffset <= $scrollPosition ) {
            $header.addClass('is-fixed');
            $body.css('margin-top', $headerHeight);
        } else {
            $header.removeClass('is-fixed');
            $body.css('margin-top', 0);
        }
 
        onScroll($scrollPosition);
    });

    function onScroll(scrollPosition) {

        $('.nav-link').each(function() {
            let $self = $(this);
            const $target = $($self.attr("href"));
            const $targetTop = $target.offset().top - 90;
            const $targetHeight = $target.height();

            if ($targetTop <= scrollPosition && $targetTop + $targetHeight > scrollPosition) {
                $('.nav-link').removeClass("nav-link-current");
                $self.addClass("nav-link-current");
            } else {
                $self.removeClass("nav-link-current");
            }
        });
    }

}
