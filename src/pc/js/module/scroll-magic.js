export default function scrollMagic() {

    // init controller


    var controller = new ScrollMagic.Controller({ globalSceneOptions: { triggerHook: "onEnter", duration: "120%" } });

    new ScrollMagic.Scene({ triggerElement: "#parallax1" })
        .setTween(".parallax-phones > .phone-1", { y: "-50%", ease: Linear.easeNone })
        .addTo(controller);


    new ScrollMagic.Scene({ triggerElement: "#parallax1" })
        .setTween(".parallax-phones > .phone-2", { y: "-10%", ease: Linear.easeNone })
        .addTo(controller);

    $(window).on("resize", function(e) {
        if ($(window).width() < 769 && controller.enabled()) {
            controller.enabled(false);
        } else if (!controller.enabled()) {
            controller.enabled(true);
        }
    });

}
